from PyPDF2 import PdfFileWriter, PdfFileReader
from time import sleep

y = input("请确认需要合并的pdf文档在与当前程序相同的目录下，并且文件名按照 1.pdf-x.pdf 排序，已确认请输入Y或y:")
if y == 'Y' or 'y':
    a = int(input("请输入要合并的pdf文件个数(即如果最后一个合并进入的pdf文件名为x.pdf，这里就输入x，x为数字哦!):"))
    pdf = PdfFileWriter()
    for i in range(a):
        pdfReader = PdfFileReader(open(str(i+1) + ".pdf", "rb"))
        print("正在处理" + str(i+1) + ".pdf文件!")
        sleep(1)
        num_page = pdfReader.getNumPages()
        for index in range(0, num_page):
            page_obj = pdfReader.getPage(index)
            pdf.addPage(page_obj)
    print("正在写入out.pdf")
    sleep(2)
    pdf.write(open("out.pdf", "wb"))
    input("任务已完成！任意键退出！")





